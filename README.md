# SteamGamesViewer

###  
How to run it:  

1. Create a file in the root folder of the project and name it `steam_api_key.txt`  
2. Insert your steam api key into the file from here:  `https://steamcommunity.com/dev/apikey`  
3. Start the server by running `python3 __main__.py`  
4. Open `http://127.0.0.1:4000/` in your browser  
